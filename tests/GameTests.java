import snake_game.*;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import static org.junit.Assert.*;

public class GameTests
{

    @Test
    public void testMovementsSize1()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        GameField field = new GameField(3,3);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        assertEquals(new Point(2, 1), game.getCurrentLevel().getSnake().getHead());

        snake.setDirection(Direction.Up);
        game.update();
        assertEquals(new Point(2, 0), game.getCurrentLevel().getSnake().getHead());

        snake.setDirection(Direction.Left);
        game.update();
        assertEquals(new Point(1, 0), game.getCurrentLevel().getSnake().getHead());

        snake.setDirection(Direction.Down);
        game.update();
        assertEquals(new Point(1,1), game.getCurrentLevel().getSnake().getHead());
    }

    @Test
    public void testMovementsSize3()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        snake_body.addFirst(new Point(3, 4));
        snake_body.addLast(new Point(2, 4));
        snake_body.addLast(new Point(1,4));
        Snake snake = new Snake(snake_body, 1, Direction.Up);
        GameField field = new GameField(5,5);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        assertEquals(new Point(3, 3), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 4)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(2, 4)));
        game.update();
        assertEquals(new Point(3, 2), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 3)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 4)));
        game.update();
        assertEquals(new Point(3, 1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 2)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 3)));

        snake.setDirection(Direction.Left);
        game.update();
        assertEquals(new Point(2, 1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 1)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 2)));
        game.update();
        assertEquals(new Point(1, 1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(2, 1)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 1)));

        snake.setDirection(Direction.Down);
        game.update();
        assertEquals(new Point(1, 2), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(1, 1)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(2, 1)));
        game.update();
        assertEquals(new Point(1, 3), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(1, 2)));
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(1, 1)));
    }

    @Test
    public void testIgnoreFieldBorder()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(4,3);
        snake_body.addFirst(new Point(3,1));
        snake_body.addLast(new Point(2, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        assertEquals(new Point(0,1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(3, 1)));
        game.update();
        assertEquals(new Point(1, 1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(0, 1)));

        snake.setDirection(Direction.Down);
        game.update();
        game.update();
        assertEquals(new Point(1, 0), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(1, 2)));
        game.update();
        assertEquals(new Point(1, 1), game.getCurrentLevel().getSnake().getHead());
        assertTrue(game.getCurrentLevel().getSnake().getBody().contains(new Point(1, 0)));
    }

    @Test
    public void testSelfConflict()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(2,3);
        snake_body.addFirst(new Point(1, 1));
        snake_body.addLast(new Point(1, 2));
        snake_body.addLast(new Point(0, 2));
        snake_body.addLast(new Point(0, 1));
        snake_body.addLast(new Point(0, 0));
        Snake snake = new Snake(snake_body, 0, Direction.Left);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        assertFalse(game.getCurrentLevel().getSnake().isAlive());
    }

    @Test
    public void testWallConflict()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(3,3);
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Up);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject wall = new GameObject(new Point(1, 0), GameObjectType.Wall);
        game.getCurrentLevel().addObject(wall);
        game.update();
        assertFalse(game.getCurrentLevel().getSnake().isAlive());
    }

    @Test
    public void testStoneConflict()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(3,3);
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject stone = new GameObject(new Point(2, 1), GameObjectType.Stone);
        game.getCurrentLevel().addObject(stone);
        game.update();
        assertFalse(game.getCurrentLevel().getSnake().isAlive());
    }

    @Test
    public void testEatApple()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(3,3);
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Down);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject apple = new GameObject(new Point(1, 2), GameObjectType.Apple);
        game.getCurrentLevel().addObject(apple);
        GameObject apple2 = new GameObject(new Point(2, 2), GameObjectType.Apple);
        game.getCurrentLevel().addObject(apple2);
        game.update();
        assertEquals(10, game.getCurrentLevel().getSnake().getScore());
        assertEquals(2, game.getCurrentLevel().getSnake().getLength());
        snake.setDirection(Direction.Right);
        game.update();
        assertEquals(20, game.getCurrentLevel().getSnake().getScore());
        assertEquals(3, game.getCurrentLevel().getSnake().getLength());
    }

    @Test
    public void testEatStrawberry()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(3,3);
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Down);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject strawberry = new GameObject(new Point(1, 2), GameObjectType.Strawberry);
        game.getCurrentLevel().addObject(strawberry);
        GameObject strawberry2 = new GameObject(new Point(2, 2), GameObjectType.Strawberry);
        game.getCurrentLevel().addObject(strawberry2);
        game.update();
        assertEquals(20, game.getCurrentLevel().getSnake().getScore());
        assertEquals(2, game.getCurrentLevel().getSnake().getLength());
        snake.setDirection(Direction.Right);
        game.update();
        assertEquals(40, game.getCurrentLevel().getSnake().getScore());
        assertEquals(3, game.getCurrentLevel().getSnake().getLength());
    }

    @Test
    public void testEatPill()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(4, 2);
        snake_body.addFirst(new Point(1, 1));
        snake_body.addLast(new Point(0, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject pill = new GameObject(new Point(2, 1), GameObjectType.Pill);
        GameObject pill2 = new GameObject(new Point(3, 1), GameObjectType.Pill);
        game.getCurrentLevel().addObject(pill);
        game.getCurrentLevel().addObject(pill2);
        game.update();
        assertEquals(1, game.getCurrentLevel().getSnake().getLength());
        assertEquals(new Point(2, 1), game.getCurrentLevel().getSnake().getHead());
        game.update();
        assertFalse(game.getCurrentLevel().getSnake().isAlive());
    }

    @Test
    public void testRandomPoints()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        snake_body.addFirst(new Point(0, 0));
        snake_body.addFirst(new Point(0, 1));
        snake_body.addFirst(new Point(0, 2));
        Snake snake = new Snake(snake_body, 0, Direction.None);
        GameField field = new GameField(1, 4);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        game.getCurrentLevel().addObject(GameObjectType.Apple);
        for (int i=0; i<1000; i++)
        {
            assertNotSame(snake.getHead(), game.getCurrentLevel().getObjects().get(0).getLocation());
            game.getCurrentLevel().getObjects().clear();
            game.getCurrentLevel().addObject(GameObjectType.Apple);
        }
    }

    @Test
    public void testTwoObjects()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(3,3);
        snake_body.addFirst(new Point(1, 1));
        Snake snake = new Snake(snake_body, 5, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject wall = new GameObject(new Point(1, 0), GameObjectType.Wall);
        GameObject apple = new GameObject(new Point(2, 1), GameObjectType.Apple);
        game.getCurrentLevel().addObject(wall);
        game.getCurrentLevel().addObject(apple);
        game.update();
        assertEquals(new Point(2, 1), game.getCurrentLevel().getSnake().getHead());
        assertEquals(new Point(1, 1), game.getCurrentLevel().getSnake().getBody().getLast());
        assertEquals(2, game.getCurrentLevel().getSnake().getLength());
        assertEquals(15, game.getCurrentLevel().getSnake().getScore());
        snake.setDirection(Direction.Up);
        game.update();
        snake.setDirection(Direction.Left);
        game.update();
        assertFalse(snake.isAlive());
    }

    @Test
    public void testThreeObjects()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(4,2);
        snake_body.addFirst(new Point(0, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject stone = new GameObject(new Point(3, 1), GameObjectType.Stone);
        GameObject apple = new GameObject(new Point(2, 1), GameObjectType.Apple);
        GameObject apple2 = new GameObject(new Point(1, 1), GameObjectType.Apple);
        game.getCurrentLevel().addObject(stone);
        game.getCurrentLevel().addObject(apple);
        game.getCurrentLevel().addObject(apple2);
        game.update();
        assertEquals(2, game.getCurrentLevel().getSnake().getLength());
        game.update();
        assertEquals(3, game.getCurrentLevel().getSnake().getLength());
        game.update();
        assertFalse(game.getCurrentLevel().getSnake().isAlive());
    }

    @Test
    public void testMoveOnTail()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(2,2);
        snake_body.addFirst(new Point(0, 1));
        snake_body.addLast(new Point(0, 0));
        snake_body.addLast(new Point(1, 0));
        snake_body.addLast(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        game.update();
        assertEquals(new Point(1, 1), snake.getHead());
        assertEquals(4, snake.getLength());
        assertTrue(snake.isAlive());
        snake.setDirection(Direction.Up);
        game.update();
        assertEquals(new Point(1, 0), snake.getHead());
        assertEquals(4, snake.getLength());
        assertTrue(snake.isAlive());
        snake.setDirection(Direction.Left);
        game.update();
        assertEquals(new Point(0, 0), snake.getHead());
        assertEquals(4, snake.getLength());
        assertTrue(snake.isAlive());
        snake.setDirection(Direction.Down);
        game.update();
        assertEquals(new Point(0,1), snake.getHead());
        assertEquals(4, snake.getLength());
        assertTrue(snake.isAlive());
    }

    @Test
    public void testInitialization()
    {
        Game game = new Game();
        assertEquals(140, game.getCurrentLevel().getObjects().size());
    }

    @Test
    public void testAppleAppearance()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(2, 2);
        snake_body.addFirst(new Point(0, 0));
        Snake snake = new Snake(snake_body, 0, Direction.Right);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject apple = new GameObject(new Point(1, 0), GameObjectType.Apple);
        game.getCurrentLevel().addObject(apple);
        game.update();
        game.controlAppleQuantity();
        assertEquals(1, game.getCurrentLevel().getObjects().size());
        assertEquals(GameObjectType.Apple, game.getCurrentLevel().getObjects().get(0).getGameObjectType());
    }

    @Test
    public void testRestart()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(8, 8);
        snake_body.addFirst(new Point(2, 2));
        Snake snake = new Snake(snake_body, 0, Direction.None);
        Game game = new Game(new Level(snake, 30, field, new ArrayList<>()));
        GameObject apple = new GameObject(new Point(1, 0), GameObjectType.Apple);
        game.getCurrentLevel().addObject(apple);
        game.restart();
        assertEquals(1, game.getCurrentLevel().getSnake().getHead().getX());
        assertEquals(1, game.getCurrentLevel().getSnake().getHead().getY());
        assertEquals(140, game.getCurrentLevel().getObjects().size());
    }

    @Test
    public void testCreatureEatsSnake()
    {
        Deque<Point> snake_body = new ArrayDeque<>();
        GameField field = new GameField(2,3);
        snake_body.addFirst(new Point(0, 1));
        snake_body.addLast(new Point(0, 0));
        snake_body.addLast(new Point(1, 0));
        snake_body.addLast(new Point(1, 1));
        Snake snake = new Snake(snake_body, 0, Direction.None);
        ArrayList<Creature> creatures = new ArrayList<>();
        ArrayList<Direction> path = new ArrayList<>();
        path.add(Direction.Up);
        Creature creature = new Monster(path, new Point(0, 2));
        creatures.add(creature);
        Game game = new Game(new Level(snake, 30, field, creatures));
        assertTrue(snake.isAlive());
        game.update();
        assertFalse(snake.isAlive());
    }
}