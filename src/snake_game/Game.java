package snake_game;

import java.util.*;

public class Game {
    private Level currentLevel;

    public void update() {
        if (!currentLevel.getSnake().isAlive())
            return;
        for (Creature creature : currentLevel.getCreatures()) {
            Point creatureTarget = getCreatureTarget(creature);
            if (creature instanceof Snake) {
                Snake snake = (Snake) creature;
                if (snake.getBody().contains(creatureTarget) && !snake.getBody().getLast().equals(creatureTarget)) {
                    creature.kill();
                    //return;
                }
            }
            Optional<GameObject> objectToRemove = Optional.empty();
            for (GameObject gameObject : currentLevel.getObjects()) {
                boolean willTouch = creatureTarget.equals(gameObject.getLocation());
                if (willTouch) {
                    if (creature instanceof Snake)
                        gameObject.makeChanges(this, creatureTarget);
                    objectToRemove = Optional.of(gameObject);
                }
            }
            if (objectToRemove.isPresent()) {
                currentLevel.getObjects().remove(objectToRemove.get());
            }
            else {
                if (!(creature instanceof Snake))
                {
                    if (currentLevel.getSnake().getBody().contains(creatureTarget) ||
                            currentLevel.getSnake().getHead().equals(creature.getLocation())) {
                        currentLevel.getSnake().kill();
                    }
                }
                creature.move(creatureTarget);
            }
            if (creature.getLength() + currentLevel.getObjects().size() <
                    currentLevel.getFieldSize().getWidth() * currentLevel.getFieldSize().getHeight())
                controlAppleQuantity();
        }
    }

    public void nextLevel() {
        currentLevel = currentLevel.getNextLevel();
    }

//    private void moveObject(GameObject object) {
//        Point target = getObjectTarget(object);
//        if (currentLevel.getSnake().getBody().contains(target))
//            currentLevel.getSnake().kill();
//        if (target.getX() == currentLevel.getFieldSize().getWidth() - 1) {
//            object.setLocation(new Point(0, target.getY()));
//            return;
//        }
//        object.setLocation(target);
//    }

    private Point getCreatureTarget(Creature creature) {
        int dx = creature.getDirection().getOffset().getX();
        int dy = creature.getDirection().getOffset().getY();
        int snakeX = creature.getLocation().getX();
        int snakeY = creature.getLocation().getY();
        return currentLevel.getFieldSize().controlBorders(snakeX + dx, snakeY + dy, creature.getLocation());
    }

//    private Point getObjectTarget(GameObject object) {
//        if (object.getGameObjectType().getDirection() == Direction.None)
//            return object.getLocation();
//        int dx = object.getGameObjectType().getDirection().getOffset().getX();
//        int dy = object.getGameObjectType().getDirection().getOffset().getY();
//        int typeX = object.getLocation().getX();
//        int typeY = object.getLocation().getY();
//        return currentLevel.getFieldSize().controlBorders(typeX + dx, typeY + dy, object.getLocation());
//    }

    public Game(Level level) {
        this.currentLevel = level;
    }

    public Game() {
        this.currentLevel = initLevels();
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    private Level initLevels() {
        List<Level> levels = new ArrayList<Level>();
        levels.add(LevelFactory.makeFirstLevel());
        levels.add(LevelFactory.makeSecondLevel());
        levels.add(LevelFactory.makeThirdLevel());
        levels.add(LevelFactory.makeFourthLevel());
        for (int i = 0; i < levels.size() - 1; i++)
            levels.get(i).setNextLevel(levels.get(i + 1));
        return levels.get(0);
    }

    public void controlAppleQuantity() {
        long apples = currentLevel.getObjects().stream()
                .filter(x -> x.getGameObjectType() == GameObjectType.Apple)
                .count();
        if (apples == 0)
            currentLevel.addObject(GameObjectType.Apple);
    }

    public void restart() {
        currentLevel = initLevels();
    }

    public void setSnakeDirection(Direction direction) {
        currentLevel.getSnake().setDirection(direction);
    }

}