package snake_game;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.function.Predicate;

public class Level
{
    private List<GameObject> objects;
    private GameField fieldSize;
    private int maxScore;
    private Level nextLevel;
    private Deque<Creature> creatures;

    public Level getNextLevel()
    {
        return nextLevel;
    }

    public Level(Snake snake, int maxScore, GameField fieldSize, List<Creature> creatures)
    {
        this.maxScore = maxScore;
        this.objects = new ArrayList<>();
        this.fieldSize = fieldSize;
        this.nextLevel = this;
        this.creatures = new ArrayDeque<>();
        this.creatures.addAll(creatures);
        this.creatures.addFirst(snake);
    }

    public void setNextLevel(Level level)
    {
        this.nextLevel = level;
    }

    public void setSnake(Snake snake)
    {
        if (creatures.getFirst() instanceof Snake)
            creatures.removeFirst();
        creatures.addFirst(snake);
    }

    public int getMaxScore()
    {
        return maxScore;
    }

    public List<GameObject> getObjects()
    {
        return objects;
    }

    public void removeObject(Point location)
    {
        for (GameObject obj : objects)
            if (obj.getLocation().equals(location))
            {
                objects.remove(obj);
                return;
            }
    }

    public void addObject(GameObject object)
    {
        objects.add(object);
    }

    public void addObject(GameObjectType objectType)
    {
        Predicate<Point> checkCollision = (randomPoint) -> getSnake().getBody().contains(randomPoint) ||
                getObjects().stream().anyMatch((o) -> o.getLocation().equals(randomPoint));
        Point newPoint = fieldSize.findFreePoint(checkCollision);
        GameObject newObj = new GameObject(newPoint, objectType);
        this.addObject(newObj);
    }

    public GameField getFieldSize()
    {
        return fieldSize;
    }

    public Deque<Creature> getCreatures(){
        return creatures;
    }

    public Snake getSnake()
    {
        Snake snake = null;
        try
        {
            snake = (Snake)creatures.toArray()[0];
        } catch (ClassCastException e)
        {
            e.printStackTrace();
        }
        return snake;
    }
}
