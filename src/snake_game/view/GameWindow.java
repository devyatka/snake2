package snake_game.view;

import snake_game.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class GameWindow extends JFrame
{
    private Game game;
    private GameBoard gameBoard;
    private String currentGameMsg;
    private String currentScore;
    private JLabel gameMsgLabel;
    private JLabel scoreLabel;

    public GameWindow(Game game, String title)
    {
        super(title);
        this.game = game;
        gameBoard = new GameBoard(game);

        gameMsgLabel = new JLabel();
        gameMsgLabel.setForeground(Color.GREEN);
        scoreLabel = new JLabel();
        scoreLabel.setForeground(Color.GREEN);
        updateMessages();
        JPanel msgPane = new JPanel();
        msgPane.setLayout(new GridLayout(2, 1));
        msgPane.add(gameMsgLabel);
        msgPane.add(scoreLabel);
        msgPane.setBackground(Color.black);

        JButton nextLvlBtn = new JButton("Next Level");
        nextLvlBtn.addActionListener(e -> onNextLvlButtonClick());
        nextLvlBtn.setBackground(Color.GRAY);
        nextLvlBtn.setForeground(Color.ORANGE);

        JPanel menuPane = new JPanel();
        menuPane.setLayout(new BorderLayout());
        menuPane.setBackground(Color.black);
        menuPane.add(nextLvlBtn, BorderLayout.WEST);
        menuPane.add(msgPane);

        this.setLayout(new BorderLayout());
        this.add(menuPane, BorderLayout.NORTH);
        this.add(gameBoard);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        setFocusable(true);
        requestFocus();
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {}

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) System.exit(0);
                gameBoard.onKeyPressed(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {}
        });
        Timer t = new Timer(100, e -> update());
        t.start();
    }

    private void updateMessages()
    {
        gameMsgLabel.setText(currentGameMsg);
        scoreLabel.setText(currentScore);
    }

    private void onNextLvlButtonClick()
    {
        game.nextLevel();
        game.update();
        updateMessages();
        requestFocus();
    }

    private void update()
    {
        if (game.getCurrentLevel().getSnake().isAlive())
        {
            game.update();
            currentGameMsg = String.format("Try to get %d points!", game.getCurrentLevel().getMaxScore());
        } else
        {
            currentGameMsg = "You lost! Try again";
            game.restart();
        }
        currentScore = String.format("Score: %d", game.getCurrentLevel().getSnake().getScore());
        super.repaint();
        updateMessages();
    }
}
