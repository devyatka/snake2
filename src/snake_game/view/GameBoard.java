package snake_game.view;

import snake_game.*;
import snake_game.Point;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;


public class GameBoard extends JPanel {
    public static int realWidth;
    public static int realHeight;
    private static final int OBJECT_SIZE = 30;

    private Game game;
    private Map<GameObjectType, BufferedImage> images;
    private BufferedImage ground;
    private BufferedImage snakeface;
    private BufferedImage body;
    private BufferedImage youwin;
    private BufferedImage monster;
    private boolean isPaused = false;

    public GameBoard(Game game) {
        this.game = game;
        realWidth = game.getCurrentLevel().getFieldSize().getWidth() * OBJECT_SIZE;
        realHeight = game.getCurrentLevel().getFieldSize().getHeight() * OBJECT_SIZE;// + 70;
        setPreferredSize(new Dimension(realWidth, realHeight));
        setBackground(Color.black);
        loadImages();
    }

    private void loadImages() {
        BufferedImage wall;
        BufferedImage apple;
        BufferedImage strawberry;
        BufferedImage pill;
        BufferedImage stone;
        try {
            apple = ImageIO.read(new File("src/snake_game/view/pics/apple.png"));
            body = ImageIO.read(new File("src/snake_game/view/pics/body.svg"));
            stone = ImageIO.read(new File("src/snake_game/view/pics/stone.png"));
            ground = ImageIO.read(new File("src/snake_game/view/pics/ground.png"));
            pill = ImageIO.read(new File("src/snake_game/view/pics/pill.png"));
            snakeface = ImageIO.read(new File("src/snake_game/view/pics/snakeface.png"));
            strawberry = ImageIO.read(new File("src/snake_game/view/pics/strawberry.gif"));
            monster = ImageIO.read(new File("src/snake_game/view/pics/monster.png"));
            wall = ImageIO.read(new File("src/snake_game/view/pics/wall.png"));
            youwin = ImageIO.read(new File("src/snake_game/view/pics/youwin.png"));
            images = new HashMap<>();
            images.put(GameObjectType.Apple, apple);
            images.put(GameObjectType.Pill, pill);
            images.put(GameObjectType.Strawberry, strawberry);
            images.put(GameObjectType.Wall, wall);
            images.put(GameObjectType.Stone, stone);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int convertToRealSize(int dimension) {
        return dimension *= OBJECT_SIZE;
    }

    private void drawImage(Graphics g, Image image, Point point) {
        g.drawImage(image, convertToRealSize(point.getX()), convertToRealSize(point.getY()), null);
    }

    public void paint(Graphics g) {
        super.paint(g);
        for (int i = 0; i < game.getCurrentLevel().getFieldSize().getWidth(); i++)
            for (int j = 0; j < game.getCurrentLevel().getFieldSize().getHeight(); j++)
                drawImage(g, ground, new Point(i, j));
        for (Creature creature : game.getCurrentLevel().getCreatures()) {
            if (creature instanceof Snake) {
                Snake snake = (Snake) creature;
                for (Point point : snake.getBody())
                {
                    drawImage(g, body, point);
                    drawImage(g, snakeface, creature.getLocation());
                }
            } else
            {
                drawImage(g, monster, creature.getLocation());
            }
        }
        for (GameObject gameObject : game.getCurrentLevel().getObjects()) {
            drawImage(g, images.get(gameObject.getGameObjectType()), gameObject.getLocation());
        }
        if (game.getCurrentLevel().getSnake().getScore() >= game.getCurrentLevel().getMaxScore())
            isPaused = true;
        if (isPaused) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, realWidth, realHeight);
            g.drawImage(youwin, realWidth / 4, 0, null);
        }
    }

    public void onKeyPressed(KeyEvent e) {
        if (isPaused) {
            isPaused = false;
            game.nextLevel();
            game.update();
        } else {
            int key = e.getKeyCode();
            setDirection(key);
            repaint();
        }
    }

    public void setDirection(int key) {
        switch (key) {
            case KeyEvent.VK_LEFT:
                game.setSnakeDirection(Direction.Left);
                break;
            case KeyEvent.VK_RIGHT:
                game.setSnakeDirection(Direction.Right);
                break;
            case KeyEvent.VK_UP:
                game.setSnakeDirection(Direction.Up);
                break;
            case KeyEvent.VK_DOWN:
                game.setSnakeDirection(Direction.Down);
        }
    }
}