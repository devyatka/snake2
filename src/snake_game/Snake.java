package snake_game;

import java.util.Deque;

public class Snake implements Creature
{
    private Deque<Point> body;
    private boolean alive;
    private int score;
    private Direction direction;

    public Snake(Deque<Point> body, int score, Direction direction)
    {
        this.body = body;
        this.alive = true;
        this.score = score;
        this.direction = direction;
    }

    public Point getHead()
    {
        return body.getFirst();
    }

    public int getLength()
    {
        return body.size();
    }

    public Deque<Point> getBody()
    {
        return body;
    }

    public void increase(Point head)
    {
        body.addFirst(head);
    }


    public void decrease()
    {
        body.removeLast();
        if (body.size() == 0)
            kill();
    }

    @Override
    public boolean isAlive()
    {
        return alive;
    }

    public int getScore()
    {
        return score;
    }


    public void addScore(int score)
    {
        this.score += score;
    }

    @Override
    public Direction getDirection()
    {
        return direction;
    }

    @Override
    public void setDirection(Direction newDirection)
    {
        Point newDir = newDirection.getOffset();
        Point currDir = getDirection().getOffset();
        if ((newDir.getX() + currDir.getX() == 0) && (newDir.getY() + currDir.getY() == 0))
            return;
        direction = newDirection;
    }

    @Override
    public void kill() {
        alive = false;
    }

    @Override
    public void move(Point target)
    {
        body.removeLast();
        body.addFirst(target);
    }

    @Override
    public Point getLocation()
    {
        return getHead();
    }

}