package snake_game;

public enum GameObjectType
{
    Strawberry(20, true, false, false),
    Apple(10, true, false, false),
    Pill(0, false, true, false),
    Wall(0, false, false, true),
    Stone(0, false, false, true);

    public final int scores;
    public final boolean increase;
    public final boolean decrease;
    public final boolean death;

    GameObjectType(int scores, boolean increase, boolean decrease, boolean death)
    {
        this.scores = scores;
        this.increase = increase;
        this.decrease = decrease;
        this.death = death;
    }

    public int getScores()
    {
        return scores;
    }

    public boolean isIncrease()
    {
        return increase;
    }

    public boolean isDecrease()
    {
        return decrease;
    }

    public boolean isDeath()
    {
        return death;
    }
}