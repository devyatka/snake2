package snake_game;

public enum Direction
{
    Up(new Point(0, -1)),
    Down(new Point(0, 1)),
    Left(new Point(-1, 0)),
    Right(new Point(1, 0)),
    None(new Point(0, 0));

    private Point offset;

    Direction(Point offset)
    {
        this.offset = offset;
    }

    public Point getOffset()
    {
        return this.offset;
    }
}