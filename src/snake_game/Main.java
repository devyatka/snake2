package snake_game;

import snake_game.view.GameBoard;
import snake_game.view.GameWindow;

import javax.swing.*;
import java.awt.*;

public class Main
{
    private static Game initGame()
    {
        return new Game();
    }

    public static void main(String[] args)
    {
        Game game = initGame();
        JFrame gameWindow = new GameWindow(game, "SNAKE");
        gameWindow.setPreferredSize(new Dimension(GameBoard.realWidth, GameBoard.realHeight));
        gameWindow.setVisible(true);
    }
}