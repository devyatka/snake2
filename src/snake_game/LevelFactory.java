package snake_game;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class LevelFactory
{
    public static Level makeFirstLevel()
    {
        ArrayList<Direction> path = makePlusPath();
        ArrayList<Creature> creatures = new ArrayList<>();
        creatures.add(new Monster(path, new Point(5, 4)));
        creatures.add(new Monster(path, new Point(20, 8)));
        creatures.add(new Monster(path, new Point(24, 18)));
        Level newLevel = new Level(initSnake(), 30,new GameField(37, 27), creatures);
        for (int i = 0; i < newLevel.getFieldSize().getWidth(); i++)
            for (int j = 0; j < newLevel.getFieldSize().getHeight(); j++)
            {
                if ((i == 0 || j == 0 || i == newLevel.getFieldSize().getWidth() - 1 ||
                        j == newLevel.getFieldSize().getHeight() - 1) && i != 5)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
            }
        newLevel.addObject(GameObjectType.Apple);
        newLevel.addObject(GameObjectType.Strawberry);
        newLevel.addObject(GameObjectType.Pill);
        for (int i = 0; i < 15; i++)
            newLevel.addObject(GameObjectType.Stone);
        return newLevel;
    }

    private static ArrayList<Direction> makePlusPath()
    {
        ArrayList<Direction> path = new ArrayList<>();
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Up);
        path.add(Direction.Up);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Up);
        path.add(Direction.Up);
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Up);
        path.add(Direction.Up);

        return path;
    }

    public static Level makeSecondLevel()
    {
        ArrayList<Direction> rightPath = makeCirclePath();
        ArrayList<Direction> leftPath = makeBackCirclePath();
        ArrayList<Creature> creatures = new ArrayList<>();
        creatures.add(new Monster(rightPath, new Point(5, 5)));
        creatures.add(new Monster(rightPath, new Point(15, 15)));
        creatures.add(new Monster(leftPath, new Point(5, 15)));
        creatures.add(new Monster(leftPath, new Point(15, 5)));
        Level newLevel = new Level(initSnake(), 50, new GameField(20, 20), creatures);
        for (int i = 0; i < 10; i++)
            newLevel.addObject(GameObjectType.Stone);
        return newLevel;
    }

    public static ArrayList<Direction> makeCirclePath()
    {
        ArrayList<Direction> path = new ArrayList<>();
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Up);
        path.add(Direction.Up);
        path.add(Direction.Up);

        return path;
    }

    private static ArrayList<Direction> makeBackCirclePath()
    {
        ArrayList<Direction> path = new ArrayList<>();
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Down);
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Right);
        path.add(Direction.Up);
        path.add(Direction.Up);
        path.add(Direction.Up);
        path.add(Direction.Left);
        path.add(Direction.Left);
        path.add(Direction.Left);

        return path;
    }

    public static Level makeThirdLevel()
    {
        ArrayList<Creature> creatures = new ArrayList<>();
        Level newLevel = new Level(initSnake(), 100, new GameField(37, 27), creatures);
        for (int i = 0; i < newLevel.getFieldSize().getWidth(); i++)
            for (int j = 0; j < newLevel.getFieldSize().getHeight(); j++)
            {
                if (i == 0 || i == newLevel.getFieldSize().getWidth() - 1
                        || j == 0 || j == newLevel.getFieldSize().getHeight() - 1)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
                if ((i == 3 || i == 33 || i == 13 || i == 23) && j > 2 && j < newLevel.getFieldSize().getHeight() - 3)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
                if ((j == 3 || j == 7 || j == 11 || j == 15 || j == 19 || j == 23) &&
                        i > 2 && i < newLevel.getFieldSize().getWidth() - 3)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
            }
        newLevel.removeObject(new Point(3, 21));
        newLevel.removeObject(new Point(23, 20));
        newLevel.removeObject(new Point(30, 19));
        newLevel.removeObject(new Point(9, 19));
        newLevel.removeObject(new Point(11, 11));
        newLevel.removeObject(new Point(23, 10));
        newLevel.removeObject(new Point(5, 7));
        newLevel.removeObject(new Point(20, 7));
        newLevel.removeObject(new Point(13, 5));
        newLevel.removeObject(new Point(13, 17));
        newLevel.removeObject(new Point(23, 17));
        newLevel.removeObject(new Point(4, 15));
        newLevel.removeObject(new Point(23, 13));
        newLevel.removeObject(new Point(28, 7));
        newLevel.removeObject(new Point(28, 11));
        for (int i = 14; i < 23; i++)
        {
            newLevel.addObject(new GameObject(new Point(i, 12), GameObjectType.Strawberry));
            newLevel.addObject(new GameObject(new Point(i, 14), GameObjectType.Strawberry));
        }
        return newLevel;
    }

    public static Level makeFourthLevel()
    {
        ArrayList<Creature> creatures = new ArrayList<>();
        Level newLevel = new Level(initSnake(), 120, new GameField(25, 15), creatures);
        for (int i = 0; i < newLevel.getFieldSize().getWidth(); i++)
            for (int j = 0; j < newLevel.getFieldSize().getHeight(); j++)
            {
                if (i == 0 || i == newLevel.getFieldSize().getWidth() - 1
                        || j == 0 || j == newLevel.getFieldSize().getHeight() - 1)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
                if (i == 3 && j < newLevel.getFieldSize().getHeight() - 3
                        || i == newLevel.getFieldSize().getWidth() - 4 && j < newLevel.getFieldSize().getHeight() - 3
                        || i == newLevel.getFieldSize().getWidth() - 7 && j > 2
                        || i == 6 && j > 2)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Wall));
                if ((i == 9 || i == 15) && j > 4 && j < 12
                        || j == 5 && i > 9 && i < 15)
                    newLevel.addObject(new GameObject(new Point(i, j), GameObjectType.Stone));
            }
        for (int i = 10; i < 15; i++)
            newLevel.addObject(new GameObject(new Point(i, 6), GameObjectType.Strawberry));
        return newLevel;
    }

    private static Snake initSnake()
    {
        Deque<Point> snake_body = new ArrayDeque<Point>();
        snake_body.addFirst(new Point(1, 1));
        return new Snake(snake_body, 0, Direction.None);
    }
}
