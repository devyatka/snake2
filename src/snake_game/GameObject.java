package snake_game;

public class GameObject
{
    public GameObjectType getGameObjectType()
    {
        return type;
    }

    public Point getLocation()
    {
        return location;
    }

    public void setLocation(Point location)
    {
        this.location = location;
    }

    private GameObjectType type;
    private Point location;

    public GameObject(Point location, GameObjectType gameObjectType)
    {
        this.location = location;
        this.type = gameObjectType;
    }

    public void makeChanges(Game game, Point target)
    {
        Snake snake = game.getCurrentLevel().getSnake();
        snake.addScore(type.getScores());
        if (type.isDeath())
        {
            snake.kill();
            return;
        }
        if (type.isIncrease())
            snake.increase(target);
        else
            snake.move(target);
        if (type.isDecrease())
            snake.decrease();
    }

}