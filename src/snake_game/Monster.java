package snake_game;

import java.util.List;

public class Monster implements Creature {

    private boolean alive;
    private Direction direction;
    private List<Direction> path;
    private Point location;

    public Monster(List<Direction> path, Point location)
    {
        alive = true;
        this.path = path;
        this.location = location;
        this.direction = (Direction) path.toArray()[0];
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }

    @Override
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public void kill() {
        alive = false;
    }

    private int stepNum = 0;

    @Override
    public void move(Point nextLocation)
    {
        location = nextLocation;
        setDirection((Direction)path.toArray()[stepNum++]);
        stepNum = stepNum % path.size();
    }

    @Override
    public int getLength() {
        return 1;
    }

    @Override
    public Point getLocation() {
        return location;
    }
}
