package snake_game;

public interface Creature
{
    boolean isAlive();
    Direction getDirection();
    void setDirection(Direction direction);
    void kill();
    void move(Point nextLocation);
    int getLength();
    Point getLocation();
}
